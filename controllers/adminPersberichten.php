<?php

class Adminpersberichten extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
    }
    
    public function index() 
    {    
        $this->view->title = 'Persberichten';
        $this->view->adminPersberichtList = $this->model->adminPersberichtList();
        
        $this->view->render('header');
        $this->view->render('adminPersberichten/index');
        $this->view->render('footer');
    }
    
    public function createPersbericht() 
    {
        $data = array();
        $data['persimage'] = $_POST['persimage'];
        $data['Titel'] = $_POST['Titel'];
        $data['Informatie'] = $_POST['Informatie'];
        
        
        // @TODO: Do your error checking!
        
        $this->model->createPersbericht($data);
        header('location: ' . URL . 'adminPersberichten');
    }
    
    public function editPersbericht($id) 
    {
        $this->view->title = 'Edit Persbericht';
        $this->view->adminPersberichten = $this->model->adminPersberichtSingleList($id);
        
        $this->view->render('header');
        $this->view->render('adminPersberichten/editPersbericht');
        $this->view->render('footer');
    }

    
    public function editSavePersberichten($id)
    {
        $data = array();
        $data['Persberichtid'] = $id;
        $data['Titel'] = $_POST['Titel'];
        $data['Informatie'] = $_POST['Informatie'];
        
        
        // @TODO: Do your error checking!
        
        $this->model->editSavePersberichten($data);
        header('location: ' . URL . 'adminPersberichten');
    }
    
    public function deletePersberichten($persberichtenid)
    {
        $this->model->deletePersberichten($persberichtenid);
        header('location: ' . URL . 'adminPersberichten');
    }
}