<?php

class Register extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {
        $this->view->title = 'Register';
        
        $this->view->render('header');
        $this->view->render('register/index');    
        $this->view->render('footer');
    }


    public function create() 
    {
        $data = array();
        $data['login'] = $_POST['login'];
        $data['password'] = $_POST['password'];
        $data['naam'] = $_POST['naam'];
        $data['email'] = $_POST['email'];
        $data['role'] = $_POST['role'];
        
        // @TODO: Do your error checking!
        
        $this->model->create($data);
        header('location: ' . URL . 'register');
    }

}