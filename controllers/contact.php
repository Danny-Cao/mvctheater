<?php

class Contact extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {
        $this->view->title = 'Contact';
        
        $this->view->render('header');
        $this->view->render('contact/index');    
        $this->view->render('footer');
    }

    function run()
    {
        
        $this->model->run();
        $this->view->render('header');
        $this->view->render('contact/succes');   
        $this->view->render('footer');

    }

}