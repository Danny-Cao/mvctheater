<?php

class Agenda extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {
        $this->view->title = 'Agenda';
        $this->view->agendaList = $this->model->agendaList();
        $this->view->render('header');
        $this->view->render('agenda/index');    
        $this->view->render('footer');
    }

    public function info($ID) 
    {

        $this->view->title = 'Voorstellingen';
        $this->view->voorstellingen = $this->model->voorstellingenList($ID);
        
        $this->view->render('header');
        $this->view->render('agenda/info');
        $this->view->render('footer');


    }


    public function bestel($ID) 
    {



        $this->view->title = 'Voorstellingen';
        $this->view->voorstellingen = $this->model->voorstellingenList($ID);
        $this->view->render('header');
        $this->view->render('agenda/bestel');
        $this->view->render('footer');
        



    }

      public function order() 
    {
        $data = $_POST;
        $this->model->updateZitplaatsen($data);

        



        
        // @TODO: Do your error checking!
        
        $this->model->order($data);
        
        $this->view->title = 'order';
        $this->view->render('header');
        $this->view->render('agenda/order');
        $this->view->render('footer');
    


    }






}