<?php

class Adminagenda extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
    }
    
    public function index() 
    {    
        $this->view->title = 'Voorstellingen';
        $this->view->adminAgendaList = $this->model->adminAgendaList();
        
        $this->view->render('header');
        $this->view->render('adminAgenda/index');
        $this->view->render('footer');
    }
    
    public function createAgenda() 
    {


        $data = array();
        $data['agendaimage'] = $_POST['agendaimage'];
        $data['Titel'] = $_POST['Titel'];
        $data['Beschrijving'] = $_POST['Beschrijving'];
        $data['zitplaatsen'] = $_POST['zitplaatsen'];
        $data['datum'] = $_POST['datum'];
        $data['tijd'] = $_POST['tijd'];
        $data['prijs'] = $_POST['prijs'];
        $data['zaal'] = $_POST['zaal'];
        $data['genre'] = $_POST['genre'];
        
        
        // @TODO: Do your error checking!
        
        $this->model->createAgenda($data);
        header('location: ' . URL . 'adminAgenda');
    }
    
    public function editAgenda($id) 
    {
        $this->view->title = 'Edit Agenda';
        $this->view->adminAgenda = $this->model->adminAgendaSingleList($id);
        
        $this->view->render('header');
        $this->view->render('adminAgenda/editAgenda');
        $this->view->render('footer');
    }
    
    public function editSaveAgenda($id)
    {
        $data = array();
        $data['ID'] = $id;
        $data['Titel'] = $_POST['Titel'];
        $data['Beschrijving'] = $_POST['Beschrijving'];
        $data['zitplaatsen'] = $_POST['zitplaatsen'];
        
        // @TODO: Do your error checking!
        
        $this->model->editSaveAgenda($data);
        header('location: ' . URL . 'adminAgenda');
    }
    
    public function deleteAgenda($ID)
    {
        $this->model->deleteAgenda($ID);
        header('location: ' . URL . 'adminAgenda');
    }
}