<?php

class Historie extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {
        $this->view->title = 'Historie';
        
        $this->view->render('header');
        $this->view->render('historie/index');    
        $this->view->render('footer');
    }

}