<?php

class Persberichten extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {
        $this->view->title = 'Persberichten';
        $this->view->persberichtenList = $this->model->persberichtenList();
        $this->view->render('header');
        $this->view->render('persberichten/index');    
        $this->view->render('footer');
    }

    function meer($persberichtid) {
        $this->view->title = 'Persberichten';
        $this->view->persSingleList = $this->model->persSingleList($persberichtid);
        $this->view->render('header');
        $this->view->render('persberichten/meer');    
        $this->view->render('footer');
    }



}