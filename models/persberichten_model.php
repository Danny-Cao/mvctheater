<?php

class Persberichten_Model extends Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function persberichtenList()
    {
        return $this->db->select('SELECT persberichtid, persimage ,Titel, Informatie FROM persberichten');
    }

    
    public function persSingleList($persberichtid)
    {
        return $this->db->select('SELECT persberichtid, persimage,Titel, Informatie FROM persberichten WHERE persberichtid = :persberichtid'
        	, array(':persberichtid' => $persberichtid));
    }




}