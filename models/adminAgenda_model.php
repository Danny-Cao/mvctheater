<?php

class Adminagenda_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function adminAgendaList()
    {
        return $this->db->select('SELECT ID, agendaimage,Titel, Beschrijving ,zitplaatsen,datum,tijd,prijs,
                                         zaal,genre,kaartjesbesteld,bezetting FROM voorstellingen');
    }
    
    public function adminAgendaSingleList($ID)
    {
        return $this->db->select('SELECT * FROM voorstellingen WHERE ID = :ID', array(':ID' => $ID));
    }
    
    public function createAgenda($data)
    {
        $this->db->insert('voorstellingen', array(
            'agendaimage'=> $data['agendaimage'],
            'Titel' => $data['Titel'],
            'Beschrijving' => $data['Beschrijving'],
            'zitplaatsen' => $data['zitplaatsen'],
            'datum' => $data['datum'],
            'tijd' => $data['tijd'],
            'prijs' => $data['prijs'],
            'zaal' => $data['zaal'],
            'genre' => $data['genre']
        ));
    }
    
    public function editSaveAgenda($data)
    {
        $postData = array(
            'Titel' => $data['Titel'],
            'Beschrijving' => $data['Beschrijving'],
            'zitplaatsen' => $data['zitplaatsen']
        );
        
        $this->db->update('voorstellingen', $postData, "`ID` = {$data['ID']}");
    }
    
    public function deleteAgenda($ID)
    {
        $result = $this->db->select('SELECT * FROM voorstellingen WHERE ID = :ID', array(':ID' => $ID));

        $this->db->delete('voorstellingen', "ID = '$ID'");
    }
}