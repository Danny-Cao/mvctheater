<?php

class Adminpersberichten_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function adminPersberichtList()
    {
        return $this->db->select('SELECT persberichtid, persimage,Titel, Informatie FROM persberichten');
    }
    
    public function adminPersberichtSingleList($persberichtid)
    {
        return $this->db->select('SELECT persberichtid, persimage,Titel, Informatie FROM persberichten WHERE  persberichtid = :persberichtid', array(':persberichtid' => $persberichtid));
    }
    
    public function createPersbericht($data)
    {
        $this->db->insert('persberichten', array(
            'persimage' => $data['persimage'],
            'Titel' => $data['Titel'],
            'Informatie' => $data['Informatie']
            
        ));
    }
    
    public function editSavePersberichten($data)
    {
        $postData = array(
            'Titel' => $data['Titel'],
            'Informatie' => $data['Informatie']
            
        );
        
        $this->db->update('persberichten', $postData, "`persberichtid` = {$data['persberichtid']}");
    }
    
    public function deletePersberichten($persberichtenid)
    {
        $result = $this->db->select('SELECT * FROM persberichten WHERE persberichtid = :persberichtid', array(':persberichtid' => $persberichtenid));

        
        
        $this->db->delete('persberichten', "persberichtid = '$persberichtenid'");
    }
}