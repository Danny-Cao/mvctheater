<?php

class Agenda_Model extends Model {

    public function __construct() {
        parent::__construct();
    }
    
     public function agendaList()
    {
        
        return $this->db->select('SELECT ID, agendaimage,datum,tijd,Titel, Beschrijving, zitplaatsen,genre,kaartjesbesteld FROM voorstellingen');
    }

    

 

    public function voorstellingenList($ID)
    {
        return $this->db->select('SELECT ID, agendaimage,Titel, Beschrijving, zitplaatsen,datum,tijd,prijs,zaal,genre
            ,kaartjesbesteld 
        FROM voorstellingen WHERE ID = :ID', array(':ID' => $ID));
    }

   
    
public function order($data) 
    {

        $this->db->insert('ticket', array(
            'datum' => $data['datum'],
            'tijd' => $data['tijd'],
            'titel' => $data['titel'],
            'naam' => $data['naam'],
            'email' => $data['email'],
            'prijs' => $data['aantal_kaartjes'] * $data['prijs'],
            'zaal' => $data['zaal'],
            'userid' => $data['userid'],
            'voorstellingid' => $data['voorstellingid'],
            'ticketcode' => $data['userid'] . $data['voorstellingid'],
            'betaalmethode' => $data['betaalmethode'],
            'aantal' => $data['aantal_kaartjes']

        ));



        $msg =  "Datum : " .$data['datum'] . 
                "\nTijd: " . $data['tijd'] . 
                "\nUw Naam: ".$data['naam'] . 
                "\nvoorstelling Titel: ".$data['titel'] . 
                "\nU heeft betaald : ".$data['prijs']. 
                "\nAantal kaartjes: ".$data['aantal_kaartjes'] . 
                "\nSpeciale Code: ".$data['userid'] . $data['voorstellingid'];



        $headers = "From: Danny.dc.cao@gmail.com";
        



        $email = $data['email'];

        $subject = 'Ticket Order:' . $data['titel'];

        mail($email,$subject,$msg,$headers);

    }

    


    
     public function updateZitplaatsen($data)
    {
        $voorstelling = $this->voorstellingenList($data['voorstelling_id']);
        $nieuweAantalZitplaatsen = (int)$voorstelling[0]['zitplaatsen'] - (int)$data['aantal_kaartjes'];

        $nieuweAantalKaartjes = (int)$data['aantal_kaartjes'];

        $this->db->update('Voorstellingen', array(
           'zitplaatsen' => $nieuweAantalZitplaatsen
        ), 'ID =' . $data['voorstelling_id']);


        
    }

    




   

}