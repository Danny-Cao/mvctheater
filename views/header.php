<!doctype html>
<html>
<head>
    <title><?=(isset($this->title)) ? $this->title : 'MVC'; ?></title>
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css" />    
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/sunny/jquery-ui.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

    <script type="text/javascript" src="<?php echo URL; ?>public/js/custom.js"></script>
   
    <?php 
    if (isset($this->js)) 
    {
        foreach ($this->js as $js)
        {
            echo '<script type="text/javascript" src="'.URL.'views/'.$js.'"></script>';
        }
    }
    ?>
</head>
<body>

<?php Session::init(); ?>
    <div id="logo">
         <a href="<?php echo URL; ?>index"></a>
    </div>
<div id="header">

    <?php if (Session::get('loggedIn') == false):?>
        <a href="<?php echo URL; ?>index">Home</a>
        <a href="<?php echo URL; ?>agenda">Agenda</a>
        <a href="<?php echo URL; ?>historie">Historie</a>
        <a href="<?php echo URL; ?>persberichten">Persberichten</a>
        <a href="<?php echo URL; ?>contact">contact</a>
        <a href="<?php echo URL; ?>register">register</a>
        <a href="<?php echo URL; ?>help">Help</a>
    <?php endif; ?>    
    <?php if (Session::get('loggedIn') == true):?>
        <a href="<?php echo URL; ?>index">Home</a>
        <a href="<?php echo URL; ?>dashboard">Dashboard</a>
        <a href="<?php echo URL; ?>note">Notes</a>
        <a href="<?php echo URL; ?>agenda">Agenda</a>
        <a href="<?php echo URL; ?>historie">Historie</a>
        <a href="<?php echo URL; ?>persberichten">Persberichten</a>
        <a href="<?php echo URL; ?>contact">contact</a>
        <a href="<?php echo URL; ?>help">Help</a>
        
        <?php if (Session::get('role') == 'owner'):?>
        <a href="<?php echo URL; ?>user">Users</a>
        <a href="<?php echo URL; ?>adminAgenda">EditAgenda</a>
        <a href="<?php echo URL; ?>adminPersberichten">EditPersberichten</a>
        
        <?php endif; ?>
        
        <a href="<?php echo URL; ?>dashboard/logout">Logout</a>    
    <?php else: ?>
        <a href="<?php echo URL; ?>login">Login</a>
    <?php endif; ?>
</div>
    
<div id="content">
    
    