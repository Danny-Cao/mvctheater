<h1>Historie</h1>

<div id="historie">

</div>

<div id="historietext">
	</br>
De Manifesto Snel en Polanen is een concertzaal die onderdeel uitmaakt van het Spuicomplex van Carel Weeber in Den Haag. 
Ook het Nederlands Danstheater van Rem Koolhaas en het Theater aan het Spui van Herman Hertzberger zijn onderdelen van het Spuicomplex. 
De Manifesto Snel en Polanen is in 1987 gebouwd naar het ontwerp van Dick van Mourik en Pieter Vermeulen.</br>

<h3>functie</h3>
In de Manifesto Snel en Polanen worden concerten gegeven in verschillende muziekstijlen, van klassieke muziek tot jazz. 
Ook worden er regelmatig concerten georganiseerd in de naburige Nieuwe Kerk.
Samen met de andere gebouwen in het Spuicomplex vormt het gebouw een cultureel zenuwpunt in de stad Den Haag.

<h3>exterieur</h3>
De Dr. Anton Philipszaal heeft een gevel bestaande uit twee delen: het ene deel bestaat uit donkerblauw spiegelend glas 
in de vorm van een liggende driehoek, 
het andere deel wordt gevormd door een gesloten wit volume met een ruitpatroon en de naam van het gebouw erop. 

<h3>interieur</h3>
De Manifesto Snel en Polanen is achter het theaterrestaurant verbonden met het Nederlands Danstheater: de twee gebouwen delen samen een foyer,
een doorgang van zeven meter breed. Naast de diverse foyers, met uitzicht op het Spui, zijn er in het complex verschillende kleedkamers 
te vinden. De zaal biedt plaats aan 1890 toeschouwers, zonder stoelen zelfs aan 2500. In de basisopstelling staan er in de zaal zelf 932 
stoelen, op de balkons 712 en in de orkestring 246. 
Het speelvlak is twintig meter breed en maximaal dertien meter diep en de voorzijde van het podium is rond.
</div>